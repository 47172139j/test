package com.example.testproject;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class MyUtilsTest {

    // Siempre void y sin parametros para testear.

    @Test
    public void testEsPrimo ()
    {


        assertEquals(false, MyUtils.esPrimo(4));
        assertEquals(false, MyUtils.esPrimo(1));

        assertTrue (MyUtils.esPrimo(2));
        assertTrue (MyUtils.esPrimo(-13));

        assertFalse (MyUtils.esPrimo(0));

    }
    @Test
    public void testInvierteCadena()
    {
        //si esto es igual a esto
        assertEquals("Hola", MyUtils.invierteCadena("aloH"));
        assertEquals("", MyUtils.invierteCadena(""));

    }

}
